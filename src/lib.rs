#![feature(asm)]
#![feature(const_fn)]
#![feature(core_intrinsics)]
#![feature(link_llvm_intrinsics)]

#[cfg(unix)]
extern crate libc;

#[cfg(windows)]
extern crate winapi;

#[allow(dead_code)]
mod safepoint;

use std::ptr::null_mut;
use std::alloc::{GlobalAlloc, Layout};

pub struct Allocator;

#[global_allocator]
pub static _DEFAULT_ALLOCATOR: Allocator = Allocator;

unsafe impl GlobalAlloc for Allocator {
    unsafe fn alloc(&self, _layout: Layout) -> *mut u8 {
        null_mut()
    }

    unsafe fn dealloc(&self, _ptr: *mut u8, _layout: Layout) {
    }
}