use std::thread::{self, JoinHandle};


pub fn spawn<F, T>(action: F) -> JoinHandle<T> where
    F: FnOnce() -> T, F: Send + 'static, T: Send + 'static {
    thread::spawn(move || {
        // TODO: save stack ptr into thread local
        // TODO: register thread into gc context
        action()
    })
}