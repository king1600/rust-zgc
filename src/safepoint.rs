pub struct Safepoint;

// Hepful link for stack unwinding later
// https://feepingcreature.github.io/handling.html

const SAFEPOINT: usize = 0x0000020000000000;
#[inline(always)] unsafe fn poll() {
    asm!("mov qword ptr [0x0000020000000000], 0" :::: "intel", "volatile");
}

extern {
    #[link_name = "llvm.stackrestore"]
    fn stack_restore(_: usize);
    #[link_name = "llvm.stacksave"]
    fn stack_save() -> usize;
}

impl Safepoint {
    #[inline(always)]
    pub fn init() {
        Self::disable();
        Self::setup_segv_handler();
    }

    #[inline(always)]
    pub fn save() -> usize {
        unsafe { stack_save() }
    }

    #[inline(always)]
    pub fn restore(ptr: usize) {
        unsafe { stack_restore(ptr) };
    }

    #[inline]
    pub fn enable() {
        let result = unsafe { Self::map_inner(true) };
        assert_eq!(result, 0, "Failed to unmap gc safepoint");
    }

    #[inline]
    pub fn disable() {
        let ptr = unsafe { Self::map_inner(false) };
        assert_eq!(ptr, SAFEPOINT, "Failed to map gc safepoint");
    }

    #[inline(always)]
    pub fn poll() {
        unsafe { poll() }
    }

    #[inline(always)]
    const fn size() -> usize {
        16 * 8 // 16 64bit registers
    }

    #[cfg(unix)]
    unsafe fn map_inner(enable: bool) -> usize {
        use libc::{mmap, munmap, c_void};
        use libc::{PROT_READ, PROT_WRITE};
        use libc::{madvise, MADV_DONTNEED};
        use libc::{MAP_FIXED, MAP_FAILED, MAP_ANONYMOUS, MAP_PRIVATE, MAP_RESERVE};

        if !enable {
            let ptr = mmap(
                SAFEPOINT as *mut c_void,
                Self::size(),
                PROT_READ | PROT_WRITE,
                MAP_FIXED | MAP_PRIVATE | MAP_RESERVE | MAP_ANONYMOUS
            );
            madvise(ptr, Self::size(), MADV_DONTNEED);
            ptr as usize
        } else {
            munmap(SAFEPOINT as *mut c_void, Self::size()) as usize
        }
    }

    #[cfg(windows)]
    unsafe fn map_inner(enable: bool) -> usize{
        use winapi::ctypes::c_void;
        use winapi::um::winnt::{MEM_RESERVE, MEM_RELEASE, PAGE_READWRITE};
        use winapi::um::memoryapi::{VirtualAlloc, VirtualFree, DiscardVirtualMemory};

        if !enable {
            let ptr = VirtualAlloc(
                SAFEPOINT as *mut c_void,
                Self::size(),
                MEM_RESERVE,
                PAGE_READWRITE
            );
            DiscardVirtualMemory(ptr, Self::size());
            ptr as usize
        } else {
            let result = VirtualFree(SAFEPOINT as *mut c_void, 0, MEM_RELEASE);
            if result != 0 { 0 } else { 1 }
        }
    }
}